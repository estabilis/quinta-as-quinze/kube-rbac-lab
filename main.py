from kubernetes import client, config

config.load_incluster_config()
v1 = client.CoreV1Api()

namespace_name = "rbac-teste"
namespace_list = [ns.metadata.name for ns in v1.list_namespace().items]

if namespace_name in namespace_list:
    print(f'{namespace_name} já existe.')
else:
    namespace = client.V1Namespace(metadata=client.V1ObjectMeta(name=namespace_name))
    v1.create_namespace(namespace)
    print("Namespace criado com sucesso:", namespace_name)

namespace_exceptions = ["default", "kube-node-lease", "kube-public", "kube-system", "kube-rbac-ns", namespace_name]
delete_namespace = set(namespace_list) - set(namespace_exceptions)

for ns in delete_namespace:
    namespace = client.CoreV1Api().delete_namespace(ns)
    print(f"Namespace {delete_namespace} excluído com sucesso!")
